# dwm-nosystray
Since the systray patch does not work well on multiple monitors, I have a version with and one without the systray patch. 



Compile dwm by running ``sudo make clean install`` in the proper directory. Then add ``exec dwm`` to the end of
your ``.xinitrc``. You can look at my ``.xinitrc`` in my dotfiles repo for an example, but you will more than likely
have to change a few things to get it working for you. If you are unfamiliar with ``xorg-xinit``, you can read 
about it [here](https://wiki.archlinux.org/title/Xinit)

## Patches

This is a comprehensive list of all the patches that were applied to this build of dwm
```
dwm-alpha-20201019-61bb8b2           
dwm-restartsig-20180523-6.2
dwm-attachaside-20180126-db22360     
dwm-rotatestack-20161021-ab9571b
dwm-cyclelayouts-20180524-6.2        
dwm-uselessgap-20200719-bb2e722
dwm-fakefullscreen-20210714-138b405
```

![image](dwm-with-conky.png)

## Dependencies:
+ libxft
+ ttf-hack
+ ttf-joypixels
+ st
+ dmenu
+ tabbed
+ Conky for the background scripts shown in the picture
+ glava for the audio visualizer
+ My scripts in my scripts repo are necessary for dwmblocks to work
+ Nerd Fonts (To display all emojis correctly)


To change any keybindings or for an exhaustive list of all of the keybindings, view/edit the ``config.h`` file.
After you make any changes, you must compile dwm again with ``sudo make clean install`` and then restart with
``MODKEY + SHIFT + r`` for changes to take effect.

## Main Keybindings

| Keybinding              | Action                                                       |
|-------------------------|--------------------------------------------------------------|
| MODKEY + RETURN         | opens terminal (I use kitty as my primary terminal)          |
| MODKEY + \ (backspace)  | opens terminal (I use st as my secondary terminal)           |
| MODKEY + SHIFT + RETURN | opens run launcher (dmenu but can be changed)                |
| MODKEY + SHIFT + c      | closes window with focus                                     |
| MODKEY + SHIFT + r      | restarts dwm                                                 |
| MODKEY + SHIFT + -      | quits dwm                                                    |
| MODKEY + b              | hides the bar                                                |
| MODKEY + 1-9            | switch focus to workspace (1-9)                              |
| MODKEY + SHIFT + 1-9    | send focused window to workspace (1-9)                       |
| MODKEY + j              | focus stack +1 (switches focus between windows in the stack) |
| MODKEY + k              | focus stack -1 (switches focus between windows in the stack) |
| MODKEY + SHIFT + j      | rotate stack +1 (rotates the windows in the stack)           |
| MODKEY + SHIFT + k      | rotate stack -1 (rotates the windows in the stack)           |
| MODKEY + h              | setmfact -0.05 (expands size of window)                      |
| MODKEY + l              | setmfact +0.05 (shrinks size of window)                      |
| MODKEY + .              | focusmon +1 (switches focus next monitors)                   |
| MODKEY + ,              | focusmon -1 (switches focus to prev monitors)                |

## Layout controls

| Keybinding             | Action                  |
|------------------------|-------------------------|
| MODKEY + d             | row layout              |
| MODKEY + i             | column layout           |
| MODKEY + TAB           | cycle layout (-1)       |
| MODKEY + SHIFT + TAB   | cycle layout (+1)       |
| MODKEY + SPACE         | change layout           |
| MODKEY + SHIFT + SPACE | toggle floating windows |
| MODKEY + t             | layout 1                |
| MODKEY + f             | layout 2                |
| MODKEY + m             | layout 3                |
| MODKEY + g             | layout 4                |

## Application controls

| Keybinding       | Action                                                                       |
|------------------|------------------------------------------------------------------------------|
| MODKEY + ALT + b | Firefox browser                                                              |
| MODKEY + ALT + c | calculator (requires qalculate-gtk)                                          |
| MODKEY + ALT + d | Discord                                                                      |
| MODKEY + ALT + f | Open File manager (Thunar)                                                   |
| MODKEY + ALT + p | Open power menu (requires my powermenu script from scripts repo)             |
| Print Screen     | Screenshot (requires spectacle)                                              |

